const apiKey = require("../page/api/index");

Given("I create new api key {string}", (apiName) => {
  apiName = apiName + Math.floor(Math.random() * 10000);
  apiKey.createAPIKey(apiName);
});

Given("I update api key {string}", (updatedName) => {
  updatedName = updatedName + Math.floor(Math.random() * 10000);
  apiKey.updateApiKey(updatedName);
});

Given("I delete api key", () => {
  apiKey.deleteApiKey();
});
