const { I } = inject();
const APILocator = require("./locator");
const timeout = require("../common/timeout");
const customMethod = require("../../utils/customMethod");
module.exports = {
  createAPIKey(apiNAME) {
    // I.waitForElement(APILocator.setupXPath, timeout.waitForElement);

    customMethod.clickElement(APILocator.setupXPath);
    customMethod.clickElement(APILocator.setupXPath);

    customMethod.clickElement(APILocator.step1);
    customMethod.clickElement(APILocator.step2);
    customMethod.fieldValue(APILocator.apiNameIpt, apiNAME);
    //customMethod.scrollToBot;
    //customMethod.clickElement(APILocator.create);
    I.pressKey("Enter");
    I.seeTextEquals(
      "API Key đã được khởi tạo thành công",
      APILocator.successMgs
    );

    I.doubleClick(APILocator.doneBtn);
  },

  updateApiKey(updateName) {
    customMethod.clickElement(APILocator.editBtn);
    customMethod.fieldValue(APILocator.apiNameIpt, updateName);
    I.pressKey("Enter");
    //I.waitForInvisible(APILocator.updateSuccessMgs);
    // I.seeTextEquals(
    //   "Cập nhật API Key thành công.",
    //   APILocator.updateSuccessMgs
    // );
  },

  deleteApiKey() {
    customMethod.clickElement(APILocator.deleteBtn, timeout);
    //I.clickElement(APILocator.deleteBtn);
    I.waitForClickable(APILocator.confirmDelBtn);
    customMethod.clickElement(APILocator.confirmDelBtn);
    I.pressKey("Enter");
    //I.seeTextEquals("Xoá API Key thành công.", APILocator.updateSuccessMgs);
  },
};
