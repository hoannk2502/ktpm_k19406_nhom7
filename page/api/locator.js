module.exports = {
  setupXPath: "//p[contains(text() , 'Thiết lập')]/../..",
  step1: "//span[contains(., 'Api keys')]",
  step2:
    "//mat-sidenav-container/mat-sidenav-content/div/app-api-keys/div/div[1]/div/button",
  apiNameIpt: "//*[contains(@id,'mat-input-')]",
  create: "//*[contains(text(),'Tạo API key')]",
  successMgs:
    "//mat-sidenav-container/mat-sidenav-content/div/app-api-keys/div/div[2]/div/div[1]/h4",
  doneBtn:
    "//mat-sidenav-container/mat-sidenav-content/div/app-api-keys/div/div[2]/div/div[3]/button",
  editBtn:
    "//mat-sidenav-container/mat-sidenav-content/div/app-api-keys/div/div[2]/mat-table/mat-row/mat-cell[3]/div/button[1]/span[1]",
  submitChange:
    "//*[contains(@id,'mat-dialog-')]/app-api-key-panel/div/div[2]/form/div/div/div[2]/button",
  updateSuccessMgs: "//div[@id  = 'toast-container']/div/div ",
  deleteBtn:
    "//mat-sidenav-container/mat-sidenav-content/div/app-api-keys/div/div[2]/mat-table/mat-row[1]/mat-cell[3]/div/button[2]",
  confirmDelBtn: "//span[normalize-space(text())='Đồng ý']",
};
